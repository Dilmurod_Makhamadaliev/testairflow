from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
import os

version = str(os.environ.get("VERSION"))


def extract():
    return 'Extracting a file'


def transform():
    return 'Transforming the file'

def transform_v2():
    return 'Transforming the file'


dag = DAG('test_'+version, description='versioning test',
          schedule_interval='0 12 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)

extract_operator = PythonOperator(task_id='extract', python_callable=extract, dag=dag)
transform_operator = PythonOperator(task_id='transform', python_callable=transform, dag=dag)
transform_operator_v2 = PythonOperator(task_id='transform_v2', python_callable=transform_v2, dag=dag)

extract_operator >> transform_operator >> transform_operator_v2
