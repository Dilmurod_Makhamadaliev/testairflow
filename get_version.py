import os

# refName = os.environ.get("CI_COMMIT_REF_NAME")
ciSHORTSHA = os.environ.get("CI_COMMIT_SHORT_SHA")
# piplineID = os.environ.get("CI_PIPELINE_ID")
# relVersion = str(refName) + ".0." + str(piplineID)
version = str(ciSHORTSHA)

# version = relVersion.replace("rel.", "")

with open('.env', 'w') as writer:
    writer.write(f'export VERSION="{version}"')
